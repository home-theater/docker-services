#!/usr/bin/env bash

if [ "$(basename $(pwd))" != "docker-services" ]; then
  echo "Execute this script from the docker-services directory"
  exit 1
fi

zip="docker-services-$(date -Iseconds).zip"

cd ..
zip -r $zip docker-services
mv $zip docker-services/
