#!/usr/bin/env bash

if [ "$(basename $(pwd))" != "docker-services" ]; then
  echo "Execute this script from the docker-services directory"
  exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
  echo "Install the docker addon first (under services)"
  exit 1
fi

DOCKER_CONF_FILE=../.kodi/userdata/addon_data/service.system.docker/config/docker.conf

if ! [ -e $DOCKER_CONF_FILE ]; then
  echo "Docker config file doesn't exist"
  exit 1
fi

grep -- --experimental $DOCKER_CONF_FILE >/dev/null
if [ $? -eq 0 ]; then
  echo "Experimental mode already enabled (restart docker if it doesn't work yet)"
else
  sed -i s/'DOCKER_DAEMON_OPTS="'/'DOCKER_DAEMON_OPTS="--experimental '/g $DOCKER_CONF_FILE
fi
