#!/usr/bin/env bash

if [ "$(basename $(pwd))" != "docker-services" ]; then
  echo "Execute this script from the docker-services directory"
  exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
  echo "Install the docker addon first (under services)"
  exit 1
fi

wget https://github.com/linuxserver/docker-docker-compose/releases/download/1.29.2-ls51/docker-compose-armhf -O /storage/.kodi/addons/service.system.docker/bin/docker-compose
chmod +x /storage/.kodi/addons/service.system.docker/bin/docker-compose

docker-compose version
