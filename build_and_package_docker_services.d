#!/usr/bin/env rdmd
import std;
import common;

import config;

static assert(__traits(compiles, template_engine_config), "template_engine_config not set");

// Run template-engine on docker-services, output in build
// Clone and build customized-addon-repo, place repository dir in build/.../addon-repository
// Copy everything in the install-guides folder to build/.../install-guides
// Zip the build dir, so it extracts as a docker-services folder

enum source_dir_name = "docker-services";
enum kodi_repo_git_url = "https://codeberg.org/home-theater/customized-kodi-repo";

int main(string[] args)
{
  if (!(args.length == 1))
  {
    writeln("Usage: ./build_and_package_docker_services.d");
    return 1;
  }
  // {"defines": [], "substitutions": {}}
  JSONValue config = parseJSON(template_engine_config);

  assert(source_dir_name.length > 0);

  string vars;
  if ("defines" in config)
    foreach (JSONValue define; config["defines"].array)
      vars ~= define.str ~ ",";
  if ("substitutions" in config)
    foreach (string key, JSONValue value; config["substitutions"].object)
      vars ~= key ~ "=" ~ value.str ~ ",";
  if (vars.endsWith(","))
    vars = vars[0 .. $ - 1];

  writeln("vars: ", vars);

  if (exists("build"))
    rmdirRecurse("build");
  mkdir("build");

  string[] buildFileList;

  // Run docker-services through template-engine, output is in build
  writeln("Running docker-services through template-engine");
  foreach (string filePath; dirEntries(source_dir_name, SpanMode.depth).map!"a.name")
  {
    string newFilePath = "build" ~ filePath[cast(long) source_dir_name.length .. $];
    if (filePath.endsWith(".template") && filePath.length > ".template".length)
    {
      newFilePath = newFilePath[0 .. $ - ".template".length];
      shell(f!"template-engine %s %s %s"(filePath, newFilePath, vars));
    }
    else
    {
      mkdirRecurse(newFilePath[0 .. $ - baseName(newFilePath).length]);
      if (isDir(filePath))
        mkdirRecurse(newFilePath);
      else
        copy(filePath, newFilePath);
    }
    buildFileList ~= newFilePath;
  }

  // Clone and build kodi-repo
  {
    string url = kodi_repo_git_url;
    string cloneDir = "repo";
    writeln("Building ", url);

    // Clone repo
    rmdirIfExists(cloneDir);
    shell(f!"git clone --recurse-submodules \"%s\" %s"(url, cloneDir));

    // Build repository tree
    {
      chdir(cloneDir);
      scope (exit)
        chdir("..");

      File("config.d", "ab").writeln(
        f!"\nstring template_engine_config = `%s`;"(template_engine_config)
      );
      shell("./create_repository.sh");
    }

    // Move repository tree
    rmdirIfExists("repository");
    shell(f!"mv %s/repository repository"(cloneDir));

    // Remove kodi-repo dir
    rmdirRecurse(cloneDir);
  }

  // Place kodi repo and everything in install-guides into build
  shell("mv repository/* build/home-theater/portal/volumes/addon-repository/");
  if (exists("install-guides") && !dirEntries("install-guides", SpanMode.shallow).empty)
    shell("cp -r install-guides/* build/home-theater/portal/volumes/install-guides/");

  rmdirRecurse("repository");

  // Warning: doesn't preserve empty directories
  ZipArchive zip = new ZipArchive();
  foreach (file; dirEntries("build", SpanMode.depth).map!"a.name")
  {
    assert(exists(file));
    if (!isDir(file))
    {
      ArchiveMember member = new ArchiveMember;
      member.compressionMethod = CompressionMethod.deflate;
      member.name = "docker-services" ~ file["build".length .. $];
      member.expandedData(cast(ubyte[]) read(file));
      zip.addMember(member);
    }
  }
  std.file.write("docker-services.zip", zip.build);

  rmdirRecurse("build");

  return 0;
}
