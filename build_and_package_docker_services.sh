#!/usr/bin/env bash

set -e

function clean {
  if [ -e common.d ]; then rm common.d; fi
}

clean

curl -Ssf https://codeberg.org/home-theater/scripts/raw/branch/master/common.d -o common.d

./build_and_package_docker_services.d

clean