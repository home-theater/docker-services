# Docker services template / zip builder

## Build

Convert the following to base64:

```json
{
  "defines": [],
  "substitutions": {
    "domain": "home-theater.example.org",
    "baseurl": "https://home-theater.example.com",
    "password": "example",
    "AllDebrid_API_KEY": "AAAAAAAAAAAAAAAAAAAA"
  }
}
```

And optinally make a "install-guides" directory with html files in it.

```
./build_and_package.d ewogICJkZWZpbmVzIjogW10sCiAgInN1YnN0aXR1dGlvbnMiOiB7CiAgICAiZG9tYWluIjogImhvbWUtdGhlYXRlci5leGFtcGxlLm9yZyIsCiAgICAiYmFzZXVybCI6ICJodHRwczovL2hvbWUtdGhlYXRlci5leGFtcGxlLmNvbSIsCiAgICAicGFzc3dvcmQiOiAiZXhhbXBsZSIsCiAgICAiQWxsRGVicmlkX0FQSV9LRVkiOiAiQUFBQUFBQUFBQUFBQUFBQUFBQUEiCiAgfQp9Cg== https://codeberg.org/home-theater/kodi-addon_recent-dl-info https://codeberg.org/home-theater/kodi-addon_remote-config
```
